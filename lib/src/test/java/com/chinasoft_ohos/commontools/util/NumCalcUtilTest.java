package com.chinasoft_ohos.commontools.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class NumCalcUtilTest {

    @Test
    public void add() {
        assertNotEquals(0.29d, 0.2d + 0.09d);

        assertEquals(0.29d, NumCalcUtil.add(0.2d, 0.09d), 0d);
        assertEquals(0.29f, NumCalcUtil.add(0.2f, 0.09f), 0f);
        assertEquals("0.29", NumCalcUtil.add("0.2", "0.09").toString());

        assertEquals("0.299", NumCalcUtil.add("0.2", "0.0991", 3));
        assertEquals("0.300", NumCalcUtil.add("0.2", "0.0995", 3));
    }

    @Test
    public void subtract() {
        assertNotEquals(0.11d, 0.2d - 0.09d);

        assertEquals(0.11d, NumCalcUtil.sub(0.2d, 0.09d), 0d);
        assertEquals(0.11f, NumCalcUtil.sub(0.2f, 0.09f), 0f);
        assertEquals("0.11", NumCalcUtil.sub("0.2", "0.09").toString());

        assertEquals("0.199", NumCalcUtil.sub("0.2", "0.0011", 3));
        assertEquals("0.198", NumCalcUtil.sub("0.2", "0.0016", 3));
    }

    @Test
    public void multiply() {
        assertNotEquals(0.18d, 0.2d * 0.9d);

        assertEquals(0.18d, NumCalcUtil.mul(0.2d, 0.9d), 0d);
        assertEquals(0.18f, NumCalcUtil.mul(0.2f, 0.9f), 0f);
        assertEquals("0.18", NumCalcUtil.mul("0.2", "0.9").toString());

        assertEquals("0.222", NumCalcUtil.mul("2.0", "0.11111", 3));
        assertEquals("0.667", NumCalcUtil.mul("6.0", "0.11111", 3));
    }

    @Test
    public void divide() {
        assertNotEquals(0.667d, 0.2d / 0.3d);

        assertEquals(0.667d, NumCalcUtil.div(0.2d, 0.3d, 3), 0d);
        assertEquals(0.667f, NumCalcUtil.div(0.2f, 0.3f, 3), 0f);
    }
}