package com.chinasoft_ohos.commontools.kt2j;

import org.junit.Test;

import static org.junit.Assert.*;

public class KtExtensionsTest {

    @Test
    public void run() {
        // run
        /*val run = kotlin.run {
            "result"
        }*/
        String run = KtExtensions.run(() -> "result");
        System.out.println("run--->" + run);

        // run with invoker
        /*val run1 = "run2".run {
            this + "result"
        }*/
        String run2 = KtExtensions.run("run2",
            invoker -> invoker + " result");
        System.out.println("run2--->" + run2);
    }

    @Test
    public void with() {
        // with
        /*val with = with("with") {
            this + "result"
        }*/
        String with = KtExtensions.with("with",
            invoker -> invoker + " result");
        System.out.println("with--->" + with);
    }

    @Test
    public void apply() {
        // apply
        /*val apply = "apply".apply {
            println("--->$this")
        }*/
        String apply = KtExtensions.apply("apply",
            invoker -> System.out.println("--->" + invoker));
        System.out.println("apply--->" + apply);
    }

    @Test
    public void also() {
        // also
        /*val also = "also".also { it->
            println("--->$it")
        }*/
        String also = KtExtensions.also("also",
            it -> System.out.println("--->" + it));
        System.out.println("also--->" + also);
    }

    @Test
    public void let() {
        // let
        /*val let = "let".let { it->
            it + "result"
        }*/
        String let = KtExtensions.let("let",
            it -> it + " result");
        System.out.println("let--->" + let);
    }

    @Test
    public void takeIf() {
        //val evenOrNull = 4.takeIf { it % 2 == 0 }
        Integer evenOrNull = KtExtensions.takeIf(4,
            it -> it % 2 == 0);
        assertEquals(4, (int) evenOrNull);

        evenOrNull = KtExtensions.takeIf(5,
            it -> it % 2 == 0);
        assertNull(evenOrNull);
    }

    @Test
    public void takeUnless() {
        Integer odd = KtExtensions.takeUnless(5,
            it -> it % 2 == 0);
        assertEquals(5, (int) odd);

        odd = KtExtensions.takeUnless(4,
            it -> it % 2 == 0);
        assertNull(odd);
    }

    @Test
    public void repeat() {
        // repeat
        /*repeat(5) { it->
            println("--->$it")
        }*/
        KtExtensions.repeat(5,
            it -> System.out.println("--->" + it));
    }
}