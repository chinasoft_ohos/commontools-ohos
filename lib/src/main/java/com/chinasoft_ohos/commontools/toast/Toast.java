/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chinasoft_ohos.commontools.toast;

import ohos.agp.utils.Color;
import ohos.app.Context;

import com.muddzdev.styleabletoast.styleabletoast.StyleableToast;


/**
 * Toast
 *
 * @since 2021-05-18
 */
public class Toast {
    public static void init(Context context) {
        if (SingletonToastBuilder.getInstance().builder == null) {
            SingletonToastBuilder.getInstance().builder = new StyleableToast.Builder(context);

            // apply default style
            SingletonToastBuilder.getInstance().builder
                    .textColor(new Color(0xFF333333))
                    .textSize(14)
                    .backgroundColor(new Color(0xFFF3F3F3));
        }
    }

    public static void show(String text) {
        StyleableToast.Builder builder = SingletonToastBuilder.getInstance().builder;

        new StyleableToast(builder.text(text))
            .show();
    }
}
