/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chinasoft_ohos.commontools.util;

import ohos.agp.components.Component;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 触摸点转换工具类
 *
 * @since 2021-04-10
 */
public class TouchHelper {
    private TouchHelper() {
    }

    /**
     * 得到触摸相对组件的左上角的偏移量X轴
     *
     * @param component 组件
     * @param event 触摸事件
     * @return 偏移量
     */
    public static float getLocalX(Component component, TouchEvent event) {
        /*
         * fixme:
         *  横屏时, pointScreenX 包含了刘海的宽度，但 componentScreenX 却没有包含，导致水平点击偏移，
         *  疑似 getLocationOnScreen 的 bug，暂无解决办法
         */
        float pointScreenX = event.getPointerScreenPosition(0).getX();
        float componentScreenX = component.getLocationOnScreen()[0];

        return pointScreenX - componentScreenX;
    }

    /**
     * 得到触摸相对组件的左上角的偏移量Y轴
     *
     * @param component 组件
     * @param event 触摸事件
     * @return 偏移量
     */
    public static float getLocalY(Component component, TouchEvent event) {
        float pointScreenY = event.getPointerScreenPosition(0).getY();
        float componentScreenY = component.getLocationOnScreen()[1];

        return pointScreenY - componentScreenY;
    }
}
