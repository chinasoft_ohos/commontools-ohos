/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chinasoft_ohos.commontools.util;

import java.util.Locale;

import timber.log.Timber;

/**
 * @since 2021-05-27
 */
public class LogTree {
    /**
     * print log by HiLog
     */
    public static class HiLog extends Timber.DebugTree {
        // do nothing
        public HiLog() {
            super(0x01);
        }

        public HiLog(int domain) {
            super(domain);
        }
    }

    /**
     * print log by System.out.println
     */
    public static class SystemOut extends Timber.DebugTree {
        @Override
        protected void log(int priority, String tag, String message, Throwable t) {
            String text = String.format(Locale.getDefault(),
                "%s/%s: %s",
                priority2Text(priority),
                tag,
                message
            );
            System.out.println(text);
        }

        private String priority2Text(int priority) {
            switch (priority) {
                default:
                case ohos.hiviewdfx.HiLog.LOG_APP:
                    return "Verbose";
                case ohos.hiviewdfx.HiLog.DEBUG:
                    return "Debug";
                case ohos.hiviewdfx.HiLog.INFO:
                    return "Info";
                case ohos.hiviewdfx.HiLog.WARN:
                    return "Warn";
                case ohos.hiviewdfx.HiLog.ERROR:
                    return "Error";
                case ohos.hiviewdfx.HiLog.FATAL:
                    return "Fatal";
            }
        }
    }
}
