CommonTools
======
ohos 三方库基础工具，包括 toast, log, ohosext, utils 等

引入方式
```groovy
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
    }
}

implementation 'com.gitee.chinasoft_ohos:commontools:0.0.4-SNAPSHOT'
```


## Timber log
特性：
 1) 自动带上当前类名称作为tag 
 2) 支持多种日志路径同时输出，比如同时以HiLog和SystemOut方式输出(以及其他扩展方式，比如：文件、网络)
```java
// 尽可能早初始化，一般放在 app 对象的 onInitialize 中
Timber.plant(new LogTree.HiLog());
//HiLog模拟器环境有时会丢失日志，可换用system.out输出，初始化方式：
// Timber.plant(new LogTree.SystemOut());

// 日志打印举例
Timber.v("SayHello");
Timber.d("onPageStart, timeMs=%d", System.currentTimeMillis());
```

## Toast
特性：外观可定制的 toast，默认外观为 AOSP 风格  
先初始化，建议放在 app 对象的 onInitialize 中：`Toast.init(getContext())`  
调用举例：`Toast.show("I am a toast like AOSP")`  

## utils
* AttrValue
  自定义组件 xml 属性读取工具类
```java
mText = AttrValue.get(attrSet, "text", "");
mTextBold = AttrValue.get(attrSet,"textBold", false);
mTextSize = AttrValue.getDimension(attrSet, "textSize", AttrHelper.fp2px(16, getContext()));
```
  
* TouchHelper
将组件的触摸事件坐标值转换为相对自身的偏移量 [详情参考](http://192.168.64.147:8099/3rd-library-team/harmony_wiki/-/issues/169)


## Kt2j - kotlin 转 java 便利工具  
 * 作用域扩展方法 KtExtensions 包括: apply, run, with, also, takeIf, takeUnless 等
 * 其他扩展方法，包括 
    * ArrayExtension：slice
    * IterableExtension: groupBy, getOrPut, joinToString
    * ...   

## ohosext - 对 ohos api 的封装和补充

* SimpleAsyncTask
使用方式和 AsyncTask 保持一致，没有 progress 进度参数

* StringPreferences
解决 ohos Preferences 不支持存储超过8192个字符问题，使用方式类似 SharedPreferences 

* ValueAnimator
下面为差异化，或者补充功能的api，其余调用方法与鸿蒙保持一致，例如`start()`,`cancel()`,`setDuration`等
```java
        // Example1
        // int和float差值动画
        ValueAnimator animator = ValueAnimator.ofFloat(0, 1f);// ValueAnimator.ofInt(0, 100);
        // component属性动画，支持旋转，缩放，平移，透明度
        ValueAnimator animator = ValueAnimator.ofObject(view, 0, 1f,
            ValueAnimator.Property.SCALE_X, ValueAnimator.Property.SCALE_Y);

        // Example2
        // int和float差值动画
        ValueAnimator animator = new ValueAnimator();
        animator.setFloatValues(0, 1f);// animator3.setIntValues(0, 100);
        // component属性动画，支持旋转，缩放，平移，透明度
        ValueAnimator animator = new ValueAnimator();
        animator.setObjectProperties(view, 0, 1f,
            ValueAnimator.Property.SCALE_X, ValueAnimator.Property.SCALE_Y);
        // 设置循环模式
        animator.setRepeatMode(ValueAnimator.RepeatMode.REVERSE);
        // 设置循环次数
        animator.setRepeatCount(AnimatorValue.INFINITE);
        // 设置差值器
        animator.setInterpolatorType(Animator.CurveType.ACCELERATE_DECELERATE);
        // 设置动画值监听器
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(AnimatorValue animatorValue, float fraction, Object animatedValue) {
                float value = (float) animatedValue;
                // animatorValue: 鸿蒙的丐版属性动画
                // fraction： 鸿蒙AnimatorValue的[0,1]
                // animatedValue: 对应android的getAnimatedValue()
            }
        });
        // 以当前为基准，反向执行动画
        animator.reverse();
        // 设置动画执行状态监听器
        animator.addListener(new ValueAnimator.AnimatorListener() {
            @Override
            public void onAnimationStart(ValueAnimator animation) {

            }

            @Override
            public void onAnimationEnd(ValueAnimator animation) {

            }

            @Override
            public void onAnimationStop(ValueAnimator animation) {

            }

            @Override
            public void onAnimationCancel(ValueAnimator animation) {

            }

            @Override
            public void onAnimationRepeat(ValueAnimator animation) {

            }

            @Override
            public void onAnimationPause(ValueAnimator animation) {

            }

            @Override
            public void onAnimationResume(ValueAnimator animation) {

            }
        });
```