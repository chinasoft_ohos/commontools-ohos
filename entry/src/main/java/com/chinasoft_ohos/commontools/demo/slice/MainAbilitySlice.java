/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chinasoft_ohos.commontools.demo.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

import com.chinasoft_ohos.commontools.toast.Toast;
import com.chinasoft_ohos.commontools.demo.ResourceTable;
import com.muddzdev.styleabletoast.styleabletoast.StyleableToast;

import timber.log.Timber;

public class MainAbilitySlice
    extends AbilitySlice
    implements Component.ClickedListener{

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Timber.tag("This is one time tag");
        Timber.d("onStart, timeMs=%d", System.currentTimeMillis());

        initView();
    }

    private void initView() {
        findComponentById(ResourceTable.Id_btn_toast_normal).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_toast_customized).setClickedListener(this);

        findComponentById(ResourceTable.Id_btn_log).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                String text = ((Text)component).getText();

                Timber.v(text);
                Timber.d(text);
                Timber.i(text);
                Timber.w(text);
                Timber.e(text);
            }
        });
    }

    @Override
    public void onClick(Component component) {
        String text = ((Text)component).getText();

        if (component.getId() == ResourceTable.Id_btn_toast_customized) {
            new StyleableToast
                .Builder(getContext())
                .text(text)
                .backgroundColor(new Color(0xFFFF5A5A))
                .cornerRadius(10)
                .show();
        } else if (component.getId() == ResourceTable.Id_btn_toast_normal) {
            Toast.show(text);
        }
    }
}
