/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chinasoft_ohos.commontools.demo;

import ohos.aafwk.ability.AbilityPackage;

import com.chinasoft_ohos.commontools.toast.Toast;
import com.chinasoft_ohos.commontools.util.LogTree;

import timber.log.Timber;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();

        // 初始化 toast
        Toast.init(getContext());
        // 可以在此修改toast的默认属性(颜色、字体等)
        //SingletonToastBuilder.getInstance().builder.textColor(new Color(0xFF000000));
        //SingletonToastBuilder.getInstance().builder.backgroundColor(new Color(0xFFFFFFFF));

        // 初始化 Timber
        Timber.plant(new LogTree.HiLog());
        //Timber.plant(new LogTree.SystemOut());
    }
}
