## 0.0.5-SNAPSHOT
* 调整 toast 样式，包括背景色和字体大小，向 emui 靠拢
* 新增 NumCalcUtil 工具类，解决 float 和 double 运算丢精度问题，可用于 findbugs 整改

## 0.0.4-SNAPSHOT
提供以下移植工具：  
* toast - 来自 StyleableToast
* log - 来自Timber  
* ohosext  
	- AttrValue: 获取 xml 属性值
	- TouchHelper: 转换点击坐标，从屏幕坐标转换为相对组件左上角坐标
	- SimpleAsyncTask： 模拟 AsyncTask
	- ValueAnimator
	- Kt2j